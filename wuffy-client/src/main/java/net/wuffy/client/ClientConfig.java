package net.wuffy.client;

import net.wuffy.common.config.Config;
import net.wuffy.core.CoreConfig;

@Config(path = "./wuffy/client.json", sourcePath = "/config/client.json")
public class ClientConfig extends CoreConfig {
}