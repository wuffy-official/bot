package net.wuffy.bot.keys;

import net.wuffy.core.lang.ITranslationKeys;

public class TranslationKeys implements ITranslationKeys {

	public static final String LANGUAGE = "language";

	//MODULE
	public static final String MODULE_COMMAND = "module:command";
	public static final String MODULE_CUSTOMCOMMAND = "module:customcommand";
	public static final String MODULE_AUTOPRUNE = "module:autoprune";
	public static final String MODULE_COUNTINGCHANNEL = "module:countingchannel";
	public static final String MODULE_POLL = "module:poll";
	public static final String MODULE_TICKETSYSTEM = "module:ticketsystem";
	public static final String MODULE_LEVELSYSTEM = "module:levelsystem";
	public static final String MODULE_NOTIFICATION = "module:notification";
	public static final String MODULE_MUSIC = "module:music";
	public static final String MODULE_GROUP = "module:group";
}